var Map = function(engine) {
	this.grid = new Grid(engine, 12, 12);

 	this.player = new Player(engine);
};

Map.prototype.update = function(engine, deltaTime) {
	this.player.update(engine, deltaTime);
};