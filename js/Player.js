var Player = function(engine) {

	this.speed = 50;

	var material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF }); 
	var radius = 15; 
	var segments = 32; 
	var circleGeometry = new THREE.CircleGeometry(radius, segments); 
	this.circle = new THREE.Mesh(circleGeometry, material); 
	this.circle.position.set(0, 0, 1);

	engine.scene.add(this.circle);

	engine.camera.follow(this.circle)
};

Player.prototype.update = function(engine, deltaTime) {
	var updateSpeed = this.speed * deltaTime

	if (engine.keyboard.pressed("up") || engine.keyboard.pressed("W")) {
		this.circle.translateY(updateSpeed);
	}
	if (engine.keyboard.pressed("down") || engine.keyboard.pressed("S")) {
		this.circle.translateY(-updateSpeed);
	}
	if (engine.keyboard.pressed("left") || engine.keyboard.pressed("A")) {
		this.circle.translateX(-updateSpeed);
	}
	if (engine.keyboard.pressed("right") || engine.keyboard.pressed("D")) {
		this.circle.translateX(updateSpeed);
	}

	// Select the grid in front of you
	if (engine.keyboard.pressed("space")) {
		// Get the grid in front of me
		var closestCell = engine.game.map.grid.getCellClosestTo(this.circle.position);

		console.log("Closest cell position: ");
		console.log(closestCell);
		console.log("My position: ");
		console.log(this.circle.position);
	}
};