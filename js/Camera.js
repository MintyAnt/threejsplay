var Camera = function() {
	this.DISTANCE = 10;

	this.width = 0;
	this.height = 0;

	this.entityToLookAt = null;

	this.initialize();
};

Camera.prototype.initialize = function() {
	this.width = window.innerWidth;
	this.height = window.innerHeight;
	var near = 0.1;
	var far = 200;
	this.camera = new THREE.OrthographicCamera( this.width / - 2, this.width / 2, this.height / 2, this.height / - 2, near, far );
	this.camera.position.z = this.DISTANCE;
};


Camera.prototype.update = function(engine, deltaTime) {
	if (this.entityToLookAt != null) {
		this.camera.position.set(this.entityToLookAt.position.x, this.entityToLookAt.position.y, this.DISTANCE);
		//console.log("looking at " + this.camera.position.x + "," + this.camera.position.y + "," + this.camera.position.z)
	}
};

Camera.prototype.follow = function(entityToLookAt) {
	this.entityToLookAt = entityToLookAt;
};

Camera.prototype.stopFollow = function(engine, deltaTime) {
	this.entityToLookAt = null;
};