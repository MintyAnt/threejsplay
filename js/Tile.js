

var Tile = function(width, height) {
	this.width = width;
	this.height = height;

	var geometry = new THREE.BoxGeometry(this.width, this.height, 1);
	var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });

	this.cube = new THREE.Mesh(geometry, material);
	this.cube.position.z = 0;
};