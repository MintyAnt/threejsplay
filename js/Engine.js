
var Engine = function() {
	this.scene = null;
	this.camera = null;
	this.renderer = null;
	this.game = null;
	this.keyboard = null;

	this.lastUpdateTime = Date.now();
};


Engine.prototype.initialize = function() {

	this.keyboard = new THREEx.KeyboardState();

	this.scene = new THREE.Scene();

	this.camera = new Camera();

	this.renderer = new THREE.WebGLRenderer();

	this.renderer.setSize(this.camera.width, this.camera.height);
	document.body.appendChild(this.renderer.domElement);
	
	this.game = new Game(this);
};

Engine.prototype.run = function() {
	this.initialize();

	var SIXTY_ISH_FPS_IN_MS = 17
	this.lastUpdateTime = Date.now();
	setInterval(this.update.bind(this), SIXTY_ISH_FPS_IN_MS);

	this.render();
};

Engine.prototype.update = function() {
    var nowTime = Date.now();
    var deltaTimeSeconds = (nowTime - this.lastUpdateTime) / 1000;
    this.lastUpdateTime = nowTime;

	this.game.update(this, deltaTimeSeconds);
	this.camera.update(this, deltaTimeSeconds);
};

Engine.prototype.render = function() {
	requestAnimationFrame(this.render.bind(this));
	this.renderer.render(this.scene, this.camera.camera);
};
