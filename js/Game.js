var Game = function(engine) {
	this.map = new Map(engine);
};

Game.prototype.update = function(engine, deltaTime) {
	this.map.update(engine, deltaTime);
};