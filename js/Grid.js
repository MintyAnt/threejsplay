
var DEFAULT_WIDTH = 40;
var DEFAULT_HEIGHT = 40;

var Grid = function(engine, gridWidth, gridHeight) {
	this.gridWidth = gridWidth
	this.gridHeight = gridHeight

	this.position = new THREE.Vector2(0, 0);

	this.tiles = [];
	this.generateTiles(engine);
};

Grid.prototype.update = function(engine, deltaTime) {

};

Grid.prototype.generateTiles = function(engine) {
	this.tiles = [];
	for (var heightIndex = 0; heightIndex < this.gridWidth; heightIndex++) {
		for (var widthIndex = 0; widthIndex < this.gridHeight; widthIndex++) {
			var newTile = new Tile(DEFAULT_WIDTH, DEFAULT_HEIGHT);

			var BUFFER = 1;

			var newTilePositionX = widthIndex * (newTile.width + BUFFER);
			var newTilePositionY = heightIndex * (newTile.height + BUFFER);
			newTilePositionX += this.position.x;
			newTilePositionY += this.position.y;

			newTile.cube.position.set(newTilePositionX, newTilePositionY, 0);

			this.tiles.push(newTile);

			engine.scene.add(newTile.cube);
		}
	}
};

Grid.prototype.getCellClosestTo = function(position) {
	if (this.tiles.length > 0 && this.tiles[0] !== undefined) {
		var relativeVectorFromOrigin = this.position.sub(new THREE.Vector2(0, 0));

		var relativeX = Math.round(position.x / DEFAULT_WIDTH);
		var relativeY = Math.round(position.y / DEFAULT_HEIGHT);
		var relativePositon = new THREE.Vector2(relativeX, relativeY);
		var withRelativeFromSpawn = relativePositon.add(relativeVectorFromOrigin);

		console.log(withRelativeFromSpawn.x + ", " + withRelativeFromSpawn.y);

		return withRelativeFromSpawn;
	} else {
		throw new Error("There were no tiles defined");
	}
};
